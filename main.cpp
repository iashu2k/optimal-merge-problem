/******************************************************************************
*File : main.cpp
*Description : Program to calculate the merge length and number of operations to merge 
               sorted files of different length using optimal merge pattern
*Author : Ashutosh Mishra and Ayush Mishra
*compiler: mingw-w64 g++ compiler version 8.1.0
*Date : Monday 20th April 2020
******************************************************************************/



#include <bits/stdc++.h>
#include "optimalMerge.h"

using namespace std;

int main(){

    optimalMerge op;//optimalMerge class object

        /******************************************************************************
*Function : input(optimalMerge)
*Description : Function to input the number of files and the size of the files
*Input parameters: no parameters
*Returns:none
******************************************************************************/
    op.input();


            /******************************************************************************
*Function : merge(optimalMerge)
*Description : Function to calculate the cost of the merge operation and number of merge operations using optimal merge
*Input parameters: no parameters
*Returns:none
******************************************************************************/
    op.merge();

           /******************************************************************************
*Function : getResult(optimalMerge)
*Description : Function to print the calculated merge length and nuber of merge operations
*Input parameters: no parameters
*Returns:none
******************************************************************************/
    op.getResult();
    
}