/******************************************************************************
*File : priorityqueue.h
*Description : Helper class to implement the priority queue
*Author : Ashutosh Mishra and Ayush Mishra
*compiler: mingw-w64 g++ compiler version 8.1.0
*Date : Monday 20th April 2020
******************************************************************************/



#include <iostream>
#include <vector>
using namespace std;

class PriorityQueue {
    vector<int> pq;
    
    public :
    
    PriorityQueue() {
        
    }
    
    bool isEmpty() {
        return pq.size() == 0;
    }
    
    
    int getSize() {
        return pq.size();
    }
    
    int getMin() {
        if(isEmpty()) {
            return 0;		
        }
        return pq[0];
    }
        /******************************************************************************
*Function : insert
*Description : Function to insert an eleent into the priority queue
*Input parameters: int element - source element to be inserted
*Returns:none
******************************************************************************/


    void insert(int element)
     {
        pq.push_back(element);
        
        int childIndex = pq.size() - 1;
        
        while(childIndex > 0) {
            int parentIndex = (childIndex - 1) / 2;
            
            if(pq[childIndex] < pq[parentIndex]) {
                int temp = pq[childIndex];
                pq[childIndex] = pq[parentIndex];
                pq[parentIndex] = temp;
            }
            else {
                break;
            }
            childIndex = parentIndex;
        }
        
    }
    
    void swap(int &a, int &b){
        int temp = a;
        a = b;
        b = temp;
    }
        /******************************************************************************
*Function : removeMin
*Description : To get and remove the minimum element from the queue
*Input parameters: no parameters
*Returns:int - minimum element form the priority queue
******************************************************************************/
    
    int removeMin()
    {
        int y = pq[0];
        pq[0] = pq[pq.size()-1];
        pq.pop_back();
        int idx = 0;
        
        while(idx*2+1<pq.size() || idx*2+2< pq.size()){
            
            if(idx*2+1<pq.size() && idx*2+2< pq.size()){
                int lc = pq[idx*2+1];
                int rc = pq[idx*2+2];
                int mini = min(lc,min(rc,pq[idx]));
                int t = -1;
                if(mini == pq[idx])
                    break;
                if(mini == lc){
                    t = idx*2 +1;
                    swap(pq[idx*2+1],pq[idx]);
                }
                
                else if(mini == rc){
                    t = idx*2 + 2;
                    
                swap(pq[idx*2+2],pq[idx]);
                }
                idx = t;
            }
            
            else if(idx*2+2<pq.size()){
                int rc = pq[idx*2+2];
                int mini = min(rc,pq[idx]); 
                int t = -1;
                if(mini==pq[idx])
                    break;
                
                if(mini == rc)
                    t = idx*2 + 2;
                
                swap(pq[idx*2+2],pq[idx]);
                idx = t;
            }
            else if(idx*2+1< pq.size()){
                int lc = pq[idx*2+1];
                int mini = min(lc,pq[idx]); 
                int t = -1;
                if(mini==pq[idx])
                    break;
                
                if(mini == lc)
                    t = idx*2 +1;
                
                swap(pq[idx*2+1],pq[idx]);
                idx = t;
            }
            
            
            
        }
        
        return y;
    }
};
    