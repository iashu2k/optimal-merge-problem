/******************************************************************************
*File : optimalMerge.h
*Description : Helper class to implement the required functions to solve the optimal merge problem
*Author : Ashutosh Mishra and Ayush Mishra
*compiler: mingw-w64 g++ compiler version 8.1.0
*Date : Monday 20th April 2020
******************************************************************************/


#include <iostream>
#include "priorityqueue.h"
#include<vector>

class optimalMerge{

    vector<int> v;
    int operations;
    int totalsize;
    public:

    /******************************************************************************
*Function : input
*Description : Function to input the number of files and the size of the files
*Input parameters: no parameters
*Returns:none
******************************************************************************/

    void input(){
        cout << "Enter number of files to merge\n";
        int n;
        cin >> n;
        int temp;
        cout << "Enter size of files\n";
        while(n--){
            cin >> temp;
            v.push_back(temp);
        }
    }
        /******************************************************************************
*Function : merge
*Description : Function to calculate the cost of the merge operation and number of merge operations using optimal merge
*Input parameters: no parameters
*Returns:none
******************************************************************************/

    void merge(){

        PriorityQueue pq;

        for(int i=0;i<v.size();i++){
            pq.insert(v[i]);

        }
        operations=0;

        while(pq.getSize() > 1){
            int first=pq.removeMin();
            int second=pq.removeMin();
            int temp=first+second;
            operations+=temp;
            pq.insert(temp);
        } 

        totalsize = pq.removeMin();
    }

        /******************************************************************************
*Function : getResult
*Description : Function to print the calculated merge length and number of merge operations
*Input parameters: no parameters
*Returns:none
******************************************************************************/

    void  getResult(){
        cout << "Total merge length =" << totalsize;
        cout << "\nMinimun cost of Merge operations =" << operations;

    }
};