### Siddaganga Institute of Technology, Tumakuru- 3

```
(An Autonomous Institute affiliated to VTU, Accredited by NBA)
2019 - 20
```
##### Department of Computer Science & Engineering

## A Report on Open ended problem titled

## “OPTIMAL MERGE PROBLEM”

##### Submitted for IV semester Analysis and Design of Algorithms LAB

##### Submitting To : Mr Prabodh C. P.

#Team Members__ Name Reg. No.

```
Ashutosh Mishra 1SI18CS018
```
```
Ayush Mishra 1SI18CS019
```

# OPTIMAL MERGE PROBLEM

Merge a set of sorted files of different length into a single sorted file. We need to find an optimal
solution, where the resultant file will be generated in minimum time.

If the number of sorted files are given, there are many ways to merge them into a single sorted
file. This merge can be performed pair wise. Hence, this type of merging is called as 2 - way
merge patterns.

As, different pairings require different amounts of time, in this strategy we want to determine an
optimal way of merging many files together. At each step, two shortest sequences are merged.

To merge a p-record file and a q-record file requires possibly p + q record moves, the obvious
choice being, merge the two smallest files together at each step.

Two-way merge patterns can be represented by binary merge trees. Let us consider a set of n
sorted files {f 1 , f 2 , f 3 **, ..., f** n}. Initially, each element of this is considered as a single node binary
tree. To find this optimal solution, the following algorithm is used.

## ALGORITHM DESIGNED

Node represents a file with a given size also given nodes are greater than 2

1. Add all the nodes in a priority queue (Min Heap).{node.weight = file size}
2. Initialize count = 0 // variable to store file computations.
3. Repeat while (size of priority Queue is greater than 1)
    a. create a new node
    b. new node = pq.poll().weight+pq.poll().weight;//pq denotes priority queue,
       remove 1st smallest and 2nd smallest element and add their weights to get
       a new node
    c. count += node.wight
    d. add this new node to priority queue;
4. count is the final answer


#### Example

Let us consider the given files, f 1 , f 2 , f 3 , f 4 and f 5 with 20, 30, 10, 5 and 30 number of elements
respectively.

If merge operations are performed according to the provided sequence, then

M 1 = merge f 1 and f 2 => 20 + 30 = 50

M 2 = merge M 1 and f 3 => 50 + 10 = 60

M 3 = merge M 2 and f 4 => 60 + 5 = 65

M 4 = merge M 3 and f 5 => 65 + 30 = 95

Hence, the total number of operations is

50 + 60 + 65 + 95 = 270

Now, the question arises is there any better solution?

Sorting the numbers according to their size in an ascending order, we get the following sequence
−

f 4 , f 3 , f 1 , f 2 , f 5

Hence, merge operations can be performed on this sequence

M 1 = merge f 4 and f 3 => 5 + 10 = 15

M 2 = merge M 1 and f 1 => 15 + 20 = 35

M 3 = merge M 2 and f 2 => 35 + 30 = 65

M 4 = merge M 3 and f 5 => 65 + 30 = 95

Therefore, the total number of operations is

15 + 35 + 65 + 95 = 210

Obviously, this is better than the previous one.



### Hence, the solution takes 15 + 35 + 60 + 95 = 205 total merge length.
### The minimum cost of Merge Operations = 365 comparisons.


